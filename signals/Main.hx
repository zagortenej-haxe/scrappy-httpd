package;

import tools.Signal;

class Main
{
    public static function main():Void
    {
        trace("Start...");
        // #if linux
        // trace("Linux");
        // #end

        var isRunning:Bool = true;

        final int_handler = function () {
            // trace('A int_handler', isRunning);
            isRunning = false;
            // trace('B int_handler', isRunning);
        }

        Signal.onInterrupt( int_handler );

        while (isRunning)
        {
            Sys.sleep(1);
        }

        trace("Exited cleanly!");
    }
}