- [] add content-length to all places that need it

- [x] add support for keep-alive so we can claim to be HTTP/1.1
    - [x] augment `process_request` to handle multiple requests and request pipelining
    - we should be able to just call `handle_request` in loop until Connection: close or 
        connection is closed, because requests will be sent on `input` and will be read
        sequentially one after another anyways

what did snake-server did for keep-alive?
https://joshblog.net/2024/introducing-snake-server-a-simple-local-web-server-for-haxe/

- [] add support for uploading files, multi-part upload

https://www.digitalocean.com/community/tutorials/http-1-1-vs-http-2-what-s-the-difference#http-1-1-pipelining-and-head-of-line-blocking

https://http.dev/1.1

https://www.rfc-editor.org/info/rfc2616

https://datatracker.ietf.org/doc/rfc2616/
