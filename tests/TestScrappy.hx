package;

import haxe.Unserializer;
import haxe.Serializer;
import haxe.io.BytesInput;
import haxe.io.Input;
import haxe.io.Bytes;
import scrappyhttpd.server.ScrappyTypes;
import scrappyhttpd.server.ScrappyHttpd;
import utest.Assert;
import utest.Test;

using TestTools;

typedef TestScrappyContent = {
    var resource: String;
    var queryParams: StringMultiMap;
}

class TestScrappyServer extends ScrappyHttpd {
    override function default_request_handler( request : HttpRequest ) : HttpResponse {
        var r : TestScrappyContent = {
            resource: request.request_line.request_target,
            queryParams: request.query_parameters,
        }
        return {
            status: 4242,
            status_text: 'passed',
            headers: request.headers,
            // headers: HeadersMultiMap.EMPTY,
            content: Bytes.ofString( Serializer.run( r ) ),
        }
    }

    public override function parse_request(input:Input):HttpRequest {
        return super.parse_request(input);
    }
}

class TestScrappy extends Test {
    static inline final LINE_SEPARATOR = '\r\n';

    function request( data : String ) : HttpResponse {
        var httpd = new TestScrappyServer("");
        var input : Input = new BytesInput( Bytes.ofString( data ) );
        var request = httpd.parse_request( input );
        return httpd.handle_request( request );
    }

    public function testQueryParams() {
        var response : HttpResponse;

        // https://datatracker.ietf.org/doc/html/rfc7230#section-3.1.1
        // response = request( "GET" );
        // Assert.equals( 400, response.status );
        Assert.raises( () -> request( "GET" ), ConnectionTimeout );

        // response = request( "GET /" );
        // Assert.equals( 400, response.status );
        Assert.raises( () -> request( "GET /" ), ConnectionTimeout );

        response = request( "GET / HTTP/2.0" + LINE_SEPARATOR );
        Assert.equals( 501, response.status );
        // Assert.raises( () -> request( "GET / HTTP/2.0" + LINE_SEPARATOR ), ConnectionTimeout );

        requestTests( 'HTTP/1.0' );
        requestTests( 'HTTP/1.1' );
    }

    function requestTests( HTTP_VERSION : String ) {
        var response : HttpResponse;
        var rc : TestScrappyContent;

        response = request( 'GET / ${HTTP_VERSION}' + LINE_SEPARATOR );
        Assert.equals( 400, response.status );

        response = request( 'GET / ${HTTP_VERSION}' + "\n" );
        Assert.equals( 400, response.status );

        response = request( 'GET / ${HTTP_VERSION}' + "\n" + LINE_SEPARATOR );
        Assert.equals( 400, response.status );

        response = request( 'GET / ${HTTP_VERSION}' + LINE_SEPARATOR + LINE_SEPARATOR );
        Assert.equals( 4242, response.status );
        rc = Unserializer.run( response.content.toString() );
        Assert.equals( "/", rc.resource );
        Assert.isTrue( rc.queryParams.isEmpty() );

        response = request( 'GET /? ${HTTP_VERSION}' + LINE_SEPARATOR + LINE_SEPARATOR );
        Assert.equals( 4242, response.status );
        rc = Unserializer.run( response.content.toString() );
        Assert.equals( "/", rc.resource );
        Assert.isTrue( rc.queryParams.isEmpty() );

        response = request( 'GET /?= ${HTTP_VERSION}' + LINE_SEPARATOR + LINE_SEPARATOR );
        Assert.equals( 4242, response.status );
        rc = Unserializer.run( response.content.toString() );
        Assert.equals( "/", rc.resource );
        Assert.isTrue( rc.queryParams.isEmpty() );

        response = request( 'GET /?foo ${HTTP_VERSION}' + LINE_SEPARATOR + LINE_SEPARATOR );
        Assert.equals( 4242, response.status );
        rc = Unserializer.run( response.content.toString() );
        Assert.equals( "/", rc.resource );
        Assert.isFalse( rc.queryParams.isEmpty() );
        Assert.equals( 1, rc.queryParams.keys().count() );
        Assert.isTrue( rc.queryParams.exists('foo') );
        Assert.equals( "", rc.queryParams.get('foo') );

        response = request( 'GET /?foo= ${HTTP_VERSION}' + LINE_SEPARATOR + LINE_SEPARATOR );
        Assert.equals( 4242, response.status );
        rc = Unserializer.run( response.content.toString() );
        Assert.equals( "/", rc.resource );
        Assert.isFalse( rc.queryParams.isEmpty() );
        Assert.equals( 1, rc.queryParams.keys().count() );
        Assert.isTrue( rc.queryParams.exists('foo') );
        Assert.equals( "", rc.queryParams.get('foo') );

        response = request( 'GET /?=foo ${HTTP_VERSION}' + LINE_SEPARATOR + LINE_SEPARATOR );
        Assert.equals( 4242, response.status );
        rc = Unserializer.run( response.content.toString() );
        Assert.equals( "/", rc.resource );
        Assert.isTrue( rc.queryParams.isEmpty() );

        response = request( 'GET /?foo=bar ${HTTP_VERSION}' + LINE_SEPARATOR + LINE_SEPARATOR );
        Assert.equals( 4242, response.status );
        rc = Unserializer.run( response.content.toString() );
        Assert.equals( "/", rc.resource );
        Assert.isFalse( rc.queryParams.isEmpty() );
        Assert.equals( 1, rc.queryParams.keys().count() );
        Assert.isTrue( rc.queryParams.exists('foo') );
        Assert.equals( "bar", rc.queryParams.get('foo') );

        response = request( 'GET /?foo-bar=baz&Foo-baR=qux ${HTTP_VERSION}' + LINE_SEPARATOR + LINE_SEPARATOR );
        Assert.equals( 4242, response.status );
        rc = Unserializer.run( response.content.toString() );
        Assert.equals( "/", rc.resource );
        Assert.isFalse( rc.queryParams.isEmpty() );
        Assert.equals( 2, rc.queryParams.keys().count() );
        Assert.isTrue( rc.queryParams.exists('foo-bar') );
        Assert.equals( "baz", rc.queryParams.get('foo-bar') );
        Assert.isTrue( rc.queryParams.exists('Foo-baR') );
        Assert.equals( "qux", rc.queryParams.get('Foo-baR') );

        response = request( 'GET /?foo=bar=baz ${HTTP_VERSION}' + LINE_SEPARATOR + LINE_SEPARATOR );
        Assert.equals( 4242, response.status );
        rc = Unserializer.run( response.content.toString() );
        Assert.equals( "/", rc.resource );
        Assert.isFalse( rc.queryParams.isEmpty() );
        Assert.equals( 1, rc.queryParams.keys().count() );
        Assert.isTrue( rc.queryParams.exists('foo') );
        Assert.equals( "bar", rc.queryParams.get('foo') );

        response = request( 'GET /?foo=bar&baz=qux ${HTTP_VERSION}' + LINE_SEPARATOR + LINE_SEPARATOR );
        Assert.equals( 4242, response.status );
        rc = Unserializer.run( response.content.toString() );
        Assert.equals( "/", rc.resource );
        Assert.isFalse( rc.queryParams.isEmpty() );
        Assert.equals( 2, rc.queryParams.keys().count() );
        Assert.isTrue( rc.queryParams.exists('foo') );
        Assert.equals( "bar", rc.queryParams.get('foo') );
        Assert.isTrue( rc.queryParams.exists('baz') );
        Assert.equals( "qux", rc.queryParams.get('baz') );

        response = request( 'GET /?foo=bar=quux&baz=qux ${HTTP_VERSION}' + LINE_SEPARATOR + LINE_SEPARATOR );
        Assert.equals( 4242, response.status );
        rc = Unserializer.run( response.content.toString() );
        Assert.equals( "/", rc.resource );
        Assert.isFalse( rc.queryParams.isEmpty() );
        Assert.equals( 2, rc.queryParams.keys().count() );
        Assert.isTrue( rc.queryParams.exists('foo') );
        Assert.equals( "bar", rc.queryParams.get('foo') );
        Assert.isTrue( rc.queryParams.exists('baz') );
        Assert.equals( "qux", rc.queryParams.get('baz') );

        response = request( 'GET /?foo=bar=quux&baz=qux#quux ${HTTP_VERSION}' + LINE_SEPARATOR + LINE_SEPARATOR );
        Assert.equals( 4242, response.status );
        rc = Unserializer.run( response.content.toString() );
        Assert.equals( "/", rc.resource );
        Assert.isFalse( rc.queryParams.isEmpty() );
        Assert.equals( 2, rc.queryParams.keys().count() );
        Assert.isTrue( rc.queryParams.exists('foo') );
        Assert.equals( "bar", rc.queryParams.get('foo') );
        Assert.isTrue( rc.queryParams.exists('baz') );
        Assert.equals( "qux", rc.queryParams.get('baz') );
    }
}
