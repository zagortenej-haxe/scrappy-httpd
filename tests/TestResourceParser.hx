package;

import scrappyhttpd.server.SegmentsNode;
import utest.Assert;
import utest.Test;

using StringTools;
using TestTools;


typedef MyHandler = Void -> String;
class MyReg extends SegmentsNode<MyHandler> {
    public function make_segments_( path_value : String ) : Array<String> {
        return make_segments( path_value );
    }
}


class TestResourceParser extends Test {

    static function print_segments( node : SegmentsNode<MyHandler>, ?indent : String ) {
        if( null == indent ) indent = '';
        Sys.print( indent );
        switch( node.type ) {
            case LITERAL: {
                Sys.print( node.value );
            }
            case PARAMETER: {
                Sys.print( '{${node.value}}' );
            }
        }
        if( node.handlers.length > 0 ) {
            Sys.print( ' - ' );
            for( h in node.handlers ) Sys.print( 'x' );
        }
        Sys.println( '' );
        for( c in node.children ) print_segments( c, indent + '  ' );
    }

    function make_regs( resource : String ) : Array<MyReg> {

        var segments = new MyReg("").make_segments_( resource );
        var regs = [for(seg in segments) new MyReg(seg)];

        return regs;
    }

    public function testSegments() {

        var regs = make_regs( "/foo/bar/baz/" );

        Assert.equals( 3, regs.length );
        Assert.equals( LITERAL, regs[0].type );
        Assert.equals( 'foo', regs[0].value );
        Assert.equals( LITERAL, regs[1].type );
        Assert.equals( 'bar', regs[1].value );
        Assert.equals( LITERAL, regs[2].type );
        Assert.equals( 'baz', regs[2].value );

        regs = make_regs( "/foo/{parm}/baz" );

        Assert.equals( 3, regs.length );
        Assert.equals( LITERAL, regs[0].type );
        Assert.equals( 'foo', regs[0].value );
        Assert.equals( PARAMETER, regs[1].type );
        Assert.equals( 'parm', regs[1].value );
        Assert.equals( LITERAL, regs[2].type );
        Assert.equals( 'baz', regs[2].value );

        regs = make_regs( "" );

        Assert.equals( 0, regs.length );

        regs = make_regs( "/" );

        Assert.equals( 1, regs.length );
        Assert.equals( LITERAL, regs[0].type );
        Assert.equals( '', regs[0].value );

        regs = make_regs( "/foo/bar baz" );

        Assert.equals( 2, regs.length );
        Assert.equals( LITERAL, regs[0].type );
        Assert.equals( 'foo', regs[0].value );
        Assert.equals( LITERAL, regs[1].type );
        Assert.equals( 'bar baz', regs[1].value );

        regs = make_regs( "/foo/" + "bar baz".urlEncode() );

        Assert.equals( 2, regs.length );
        Assert.equals( LITERAL, regs[0].type );
        Assert.equals( 'foo', regs[0].value );
        Assert.equals( LITERAL, regs[1].type );
        Assert.equals( 'bar baz'.urlEncode(), regs[1].value );
    }

    public function testSmoke() {

        var root = new MyReg();
        root.register( "/foo/{parm}/baz", ()->'A' );
        root.register( "/foo/bar/baz/", ()->'B' );
        root.register( "/qux/baz", ()->'C' );
        root.register( "/qux/quz/", ()->'D' );
        root.register( "/qux/quz/{param}", ()->'E' );
        // print_segments( root );

        var r;

        // "" is root by default
        r = root.getHandlers( "" );
        Assert.notNull( r );
        Assert.isTrue( r.params.isEmpty() );
        Assert.equals( 0, r.handlers.length );

        r = root.getHandlers( "/" );
        Assert.isNull( r );

        r = root.getHandlers( "/bar" );
        Assert.isNull( r );

        r = root.getHandlers( "/foo/" );
        Assert.notNull( r );
        Assert.isTrue( r.params.isEmpty() );
        Assert.equals( 0, r.handlers.length );

        r = root.getHandlers( "/foo/bar" );
        Assert.notNull( r );
        Assert.isTrue( r.params.isEmpty() );
        Assert.equals( 0, r.handlers.length );

        r = root.getHandlers( "/foo/bar/baz/quux" );
        Assert.isNull( r );

        r = root.getHandlers( "/foo/{parm}/baz" );
        Assert.notNull( r );
        Assert.isTrue( r.handlers.length > 0 );
        Assert.equals( 'A', r.handlers[0]() );
        Assert.equals( 1, r.params.keys().count() );
        Assert.isTrue( r.params.exists('parm') );
        Assert.equals( '{parm}', r.params.get('parm') );

        r = root.getHandlers( "/foo/bar/baz" );
        Assert.notNull( r );
        Assert.isTrue( r.handlers.length > 0 );
        Assert.equals( 'B', r.handlers[0]() );
        Assert.isTrue( r.params.isEmpty() );

        r = root.getHandlers( "/foo/bar/baz/" );
        Assert.notNull( r );
        Assert.isTrue( r.handlers.length > 0 );
        Assert.equals( 'B', r.handlers[0]() );
        Assert.isTrue( r.params.isEmpty() );

        root.register( "/", ()->'F' );

        r = root.getHandlers( "/" );
        Assert.notNull( r );
        Assert.isTrue( r.params.isEmpty() );
        Assert.isTrue( r.handlers.length > 0 );
        Assert.equals( 'F', r.handlers[0]() );

        r = root.getHandlers( "/foo/bar/baz" );
        Assert.notNull( r );
        Assert.isTrue( r.handlers.length > 0 );
        Assert.equals( 'B', r.handlers[0]() );
        Assert.isTrue( r.params.isEmpty() );

        // should not be able to mask root "" (non)handler
        root.register( "", ()->'G' );
        r = root.getHandlers( "" );
        Assert.notNull( r );
        Assert.isTrue( r.params.isEmpty() );
        Assert.equals( 0, r.handlers.length );
    }

}
