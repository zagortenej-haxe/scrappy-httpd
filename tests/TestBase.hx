package;

import haxe.Unserializer;
import haxe.Serializer;
import haxe.io.BytesInput;
import haxe.io.Input;
import haxe.io.Bytes;
import scrappyhttpd.server.ScrappyTypes;
import scrappyhttpd.server.ScrappyHttpdBase;
import utest.Assert;
import utest.Test;

using TestTools;

typedef TestResponseContent = {
    var resource: String;
    var queryString: String;
    var queryFragment: String;
    var queryParams: StringMultiMap;
}

class TestServer extends ScrappyHttpdBase {
    override function default_request_handler( request : HttpRequest ) : HttpResponse {
        var r : TestResponseContent = {
            resource: request.request_line.request_target,
            queryString: request.query,
            queryFragment: request.fragment,
            queryParams: request.query_parameters,
        }
        return {
            status: 4242,
            status_text: 'passed',
            headers: request.headers,
            // headers: HeadersMultiMap.EMPTY,
            content: Bytes.ofString( Serializer.run( r ) ),
        }
    }

    public override function parse_request(input:Input):HttpRequest {
        return super.parse_request(input);
    }
}

class TestBase extends Test {
    static inline final LINE_SEPARATOR = '\r\n';

    function request( data : String ) : HttpResponse {
        var httpd = new TestServer();
        var input : Input = new BytesInput( Bytes.ofString( data ) );
        var request = httpd.parse_request( input );
        return httpd.handle_request( request );
    }

    public function testRequestLine() {
        var response : HttpResponse;
        var rc : TestResponseContent;

        // https://datatracker.ietf.org/doc/html/rfc7230#section-3.1.1
        // response = request( "GET" );
        // Assert.equals( 400, response.status );
        Assert.raises( () -> request( "GET" ), ConnectionTimeout );

        // response = request( "GET /" );
        // Assert.equals( 400, response.status );
        Assert.raises( () -> request( "GET /" ), ConnectionTimeout );

        response = request( "GET / HTTP/2.0" + LINE_SEPARATOR );
        Assert.equals( 501, response.status );

        response = request( "GET / HTTP/1.2" + LINE_SEPARATOR );
        Assert.equals( 400, response.status );


        requestLineTests( 'HTTP/1.0' );
        requestLineTests( 'HTTP/1.1' );
    }

    function requestLineTests( HTTP_VERSION : String ) {
        var response : HttpResponse;
        var rc : TestResponseContent;

        response = request( 'GET / ${HTTP_VERSION}' );
        Assert.equals( 400, response.status );

        response = request( 'GET / ${HTTP_VERSION}' + "\n" );
        Assert.equals( 400, response.status );

        response = request( 'GET / ${HTTP_VERSION}' + LINE_SEPARATOR );
        Assert.equals( 400, response.status );

        response = request( 'GET / ${HTTP_VERSION}' + "\n" + LINE_SEPARATOR );
        Assert.equals( 400, response.status );

        response = request( 'GET / ${HTTP_VERSION}' + LINE_SEPARATOR + LINE_SEPARATOR );
        Assert.equals( 4242, response.status );
        rc = Unserializer.run( response.content.toString() );
        Assert.equals( "/", rc.resource );
        Assert.equals( "", rc.queryString );
        Assert.isTrue( rc.queryParams.isEmpty() );

        response = request( ' GET / ${HTTP_VERSION}' + LINE_SEPARATOR );
        Assert.equals( 400, response.status );

        response = request( 'GET  / ${HTTP_VERSION}' + LINE_SEPARATOR );
        Assert.equals( 400, response.status );

        response = request( 'GET /  ${HTTP_VERSION}' + LINE_SEPARATOR );
        Assert.equals( 400, response.status );

        response = request( 'GET /? ${HTTP_VERSION}' + LINE_SEPARATOR + LINE_SEPARATOR );
        Assert.equals( 4242, response.status );
        rc = Unserializer.run( response.content.toString() );
        Assert.equals( "/", rc.resource );
        Assert.equals( "", rc.queryString );
        Assert.equals( "", rc.queryFragment );
        Assert.isTrue( rc.queryParams.isEmpty() );

        response = request( 'GET /# ${HTTP_VERSION}' + LINE_SEPARATOR + LINE_SEPARATOR );
        Assert.equals( 4242, response.status );
        rc = Unserializer.run( response.content.toString() );
        Assert.equals( "/", rc.resource );
        Assert.equals( "", rc.queryString );
        Assert.equals( "", rc.queryFragment );
        Assert.isTrue( rc.queryParams.isEmpty() );

        response = request( 'GET /?= ${HTTP_VERSION}' + LINE_SEPARATOR + LINE_SEPARATOR );
        Assert.equals( 4242, response.status );
        rc = Unserializer.run( response.content.toString() );
        Assert.equals( "/", rc.resource );
        Assert.equals( "=", rc.queryString );
        Assert.equals( "", rc.queryFragment );
        Assert.isTrue( rc.queryParams.isEmpty() );

        response = request( 'GET /?=# ${HTTP_VERSION}' + LINE_SEPARATOR + LINE_SEPARATOR );
        Assert.equals( 4242, response.status );
        rc = Unserializer.run( response.content.toString() );
        Assert.equals( "/", rc.resource );
        Assert.equals( "=", rc.queryString );
        Assert.equals( "", rc.queryFragment );
        Assert.isTrue( rc.queryParams.isEmpty() );

        response = request( 'GET /?# ${HTTP_VERSION}' + LINE_SEPARATOR + LINE_SEPARATOR );
        Assert.equals( 4242, response.status );
        rc = Unserializer.run( response.content.toString() );
        Assert.equals( "/", rc.resource );
        Assert.equals( "", rc.queryString );
        Assert.equals( "", rc.queryFragment );
        Assert.isTrue( rc.queryParams.isEmpty() );

        response = request( 'GET /?foo ${HTTP_VERSION}' + LINE_SEPARATOR + LINE_SEPARATOR );
        Assert.equals( 4242, response.status );
        rc = Unserializer.run( response.content.toString() );
        Assert.equals( "/", rc.resource );
        Assert.equals( "foo", rc.queryString );
        Assert.equals( "", rc.queryFragment );
        Assert.isTrue( rc.queryParams.isEmpty() );

        response = request( 'GET /?foo= ${HTTP_VERSION}' + LINE_SEPARATOR + LINE_SEPARATOR );
        Assert.equals( 4242, response.status );
        rc = Unserializer.run( response.content.toString() );
        Assert.equals( "/", rc.resource );
        Assert.equals( "foo=", rc.queryString );
        Assert.equals( "", rc.queryFragment );
        Assert.isTrue( rc.queryParams.isEmpty() );

        response = request( 'GET /?=foo ${HTTP_VERSION}' + LINE_SEPARATOR + LINE_SEPARATOR );
        Assert.equals( 4242, response.status );
        rc = Unserializer.run( response.content.toString() );
        Assert.equals( "/", rc.resource );
        Assert.equals( "=foo", rc.queryString );
        Assert.equals( "", rc.queryFragment );
        Assert.isTrue( rc.queryParams.isEmpty() );

        response = request( 'GET /?foo=bar ${HTTP_VERSION}' + LINE_SEPARATOR + LINE_SEPARATOR );
        Assert.equals( 4242, response.status );
        rc = Unserializer.run( response.content.toString() );
        Assert.equals( "/", rc.resource );
        Assert.equals( "foo=bar", rc.queryString );
        Assert.equals( "", rc.queryFragment );
        Assert.isTrue( rc.queryParams.isEmpty() );

        response = request( 'GET /?foo-bar=baz&Foo-baR=qux ${HTTP_VERSION}' + LINE_SEPARATOR + LINE_SEPARATOR );
        Assert.equals( 4242, response.status );
        rc = Unserializer.run( response.content.toString() );
        Assert.equals( "/", rc.resource );
        Assert.equals( "foo-bar=baz&Foo-baR=qux", rc.queryString );
        Assert.equals( "", rc.queryFragment );
        Assert.isTrue( rc.queryParams.isEmpty() );

        response = request( 'GET /?foo=bar=baz ${HTTP_VERSION}' + LINE_SEPARATOR + LINE_SEPARATOR );
        Assert.equals( 4242, response.status );
        rc = Unserializer.run( response.content.toString() );
        Assert.equals( "/", rc.resource );
        Assert.equals( "foo=bar=baz", rc.queryString );
        Assert.equals( "", rc.queryFragment );
        Assert.isTrue( rc.queryParams.isEmpty() );

        response = request( 'GET /?foo=bar&baz=qux ${HTTP_VERSION}' + LINE_SEPARATOR + LINE_SEPARATOR );
        Assert.equals( 4242, response.status );
        rc = Unserializer.run( response.content.toString() );
        Assert.equals( "/", rc.resource );
        Assert.equals( "foo=bar&baz=qux", rc.queryString );
        Assert.equals( "", rc.queryFragment );
        Assert.isTrue( rc.queryParams.isEmpty() );

        response = request( 'GET /?foo=bar=quux&baz=qux ${HTTP_VERSION}' + LINE_SEPARATOR + LINE_SEPARATOR );
        Assert.equals( 4242, response.status );
        rc = Unserializer.run( response.content.toString() );
        Assert.equals( "/", rc.resource );
        Assert.equals( "foo=bar=quux&baz=qux", rc.queryString );
        Assert.equals( "", rc.queryFragment );
        Assert.isTrue( rc.queryParams.isEmpty() );

        response = request( 'GET /?foo=bar=quux&baz=qux#quux ${HTTP_VERSION}' + LINE_SEPARATOR + LINE_SEPARATOR );
        Assert.equals( 4242, response.status );
        rc = Unserializer.run( response.content.toString() );
        Assert.equals( "/", rc.resource );
        Assert.equals( "foo=bar=quux&baz=qux", rc.queryString );
        Assert.equals( "quux", rc.queryFragment );
        Assert.isTrue( rc.queryParams.isEmpty() );
    }

    public function testHeaders() {
        var response : HttpResponse;

        // response = request(
        //     "GET / HTTP/1.1" + LINE_SEPARATOR +
        //     "header-name: value"
        // );
        // Assert.equals( 400, response.status );
        Assert.raises( () -> request(
            "GET / HTTP/1.1" + LINE_SEPARATOR +
            "header-name: value"
        ), ConnectionTimeout );

        // response = request(
        //     "GET / HTTP/1.1" + LINE_SEPARATOR +
        //     "header-name: value" + LINE_SEPARATOR
        // );
        // Assert.equals( 400, response.status );
        Assert.raises( () -> request(
            "GET / HTTP/1.1" + LINE_SEPARATOR +
            "header-name: value" + LINE_SEPARATOR
        ), ConnectionTimeout );

        response = request(
            "GET / HTTP/1.1" + LINE_SEPARATOR +
            "header-name: value" + LINE_SEPARATOR +
            LINE_SEPARATOR
        );
        Assert.equals( 4242, response.status );
        Assert.isFalse( response.headers.isEmpty() );
        Assert.equals( 1, response.headers.keys().count() );
        Assert.same( "value", response.headers.get('header-name') );
        Assert.same( ["value"], response.headers.getAll('header-name') );

        // response = request(
        //     "GET / HTTP/1.1" + LINE_SEPARATOR +
        //     "header-name: value" + "\r" +
        //     LINE_SEPARATOR
        // );
        // Assert.equals( 400, response.status );
        Assert.raises( () -> request(
            "GET / HTTP/1.1" + LINE_SEPARATOR +
            "header-name: value" + "\r" +
            LINE_SEPARATOR
        ), ConnectionTimeout );

        response = request(
            "GET / HTTP/1.1" + LINE_SEPARATOR +
            "header-name: value" + "\n" +
            LINE_SEPARATOR
        );
        Assert.equals( 400, response.status );

        response = request(
            "GET / HTTP/1.1" + LINE_SEPARATOR +
            "header-name: value" + LINE_SEPARATOR + "\n" +
            LINE_SEPARATOR
        );
        Assert.equals( 400, response.status );

        response = request(
            "GET / HTTP/1.1" + LINE_SEPARATOR +
            "header-name: value" + LINE_SEPARATOR + "\r" +
            LINE_SEPARATOR
        );
        Assert.equals( 400, response.status );

        response = request(
            "GET / HTTP/1.1" + LINE_SEPARATOR +
            " header-name: value" + LINE_SEPARATOR +
            LINE_SEPARATOR
        );
        Assert.equals( 400, response.status );

        response = request(
            "GET / HTTP/1.1" + LINE_SEPARATOR +
            "header-name : value" + LINE_SEPARATOR +
            LINE_SEPARATOR
        );
        Assert.equals( 400, response.status );

        response = request(
            "GET / HTTP/1.1" + LINE_SEPARATOR +
            "header-name: value " + LINE_SEPARATOR +
            LINE_SEPARATOR
        );
        Assert.equals( 4242, response.status );
        Assert.isFalse( response.headers.isEmpty() );
        Assert.equals( 1, response.headers.keys().count() );
        Assert.same( "value", response.headers.get('header-name') );
        Assert.same( ["value"], response.headers.getAll('header-name') );

        response = request(
            "GET / HTTP/1.1" + LINE_SEPARATOR +
            "name: value " + LINE_SEPARATOR +
            LINE_SEPARATOR
        );
        Assert.equals( 4242, response.status );
        Assert.isFalse( response.headers.isEmpty() );
        Assert.equals( 1, response.headers.keys().count() );
        Assert.same( "value", response.headers.get('name') );
        Assert.same( ["value"], response.headers.getAll('name') );

        response = request(
            "GET / HTTP/1.1" + LINE_SEPARATOR +
            "header-name: foo" + LINE_SEPARATOR +
            "header-name: bar" + LINE_SEPARATOR +
            LINE_SEPARATOR
        );
        Assert.equals( 4242, response.status );
        Assert.isFalse( response.headers.isEmpty() );
        Assert.equals( 1, response.headers.keys().count() );
        Assert.same( "foo", response.headers.get('header-name') );
        Assert.same( ["foo","bar"], response.headers.getAll('header-name') );

        response = request(
            "GET / HTTP/1.1" + LINE_SEPARATOR +
            "header-name: foo" + LINE_SEPARATOR +
            "HEADER-name: bar" + LINE_SEPARATOR +
            LINE_SEPARATOR
        );
        Assert.equals( 4242, response.status );
        Assert.isFalse( response.headers.isEmpty() );
        Assert.equals( 1, response.headers.keys().count() );
        Assert.same( "foo", response.headers.get('header-name') );
        Assert.same( "foo", response.headers.get('Header-Name') );
        Assert.same( "foo", response.headers.get('HEADER-NAME') );
        Assert.same( ["foo","bar"], response.headers.getAll('header-name') );

        response = request(
            "GET / HTTP/1.1" + LINE_SEPARATOR +
            "header-name: bar" + LINE_SEPARATOR +
            "header-name: foo" + LINE_SEPARATOR +
            LINE_SEPARATOR
        );
        Assert.equals( 4242, response.status );
        Assert.isFalse( response.headers.isEmpty() );
        Assert.equals( 1, response.headers.keys().count() );
        Assert.same( "bar", response.headers.get('header-name') );
        Assert.same( ["bar","foo"], response.headers.getAll('header-name') );
    }

    public function testPath() {
        var response : HttpResponse;
        var rc : TestResponseContent;

        response = request( "GET / HTTP/1.1" + LINE_SEPARATOR + LINE_SEPARATOR );
        Assert.equals( 4242, response.status );
        rc = Unserializer.run( response.content.toString() );
        Assert.equals( "/", rc.resource );

        response = request( "GET /foo HTTP/1.1" + LINE_SEPARATOR + LINE_SEPARATOR );
        Assert.equals( 4242, response.status );
        rc = Unserializer.run( response.content.toString() );
        Assert.equals( "/foo", rc.resource );

        response = request( "GET foo HTTP/1.1" + LINE_SEPARATOR + LINE_SEPARATOR );
        Assert.equals( 400, response.status );

        response = request( "GET /foo/ HTTP/1.1" + LINE_SEPARATOR + LINE_SEPARATOR );
        Assert.equals( 4242, response.status );
        rc = Unserializer.run( response.content.toString() );
        Assert.equals( "/foo/", rc.resource );

        response = request( "GET /foo/? HTTP/1.1" + LINE_SEPARATOR + LINE_SEPARATOR );
        Assert.equals( 4242, response.status );
        rc = Unserializer.run( response.content.toString() );
        Assert.equals( "/foo/", rc.resource );

        response = request( "GET /foo/bar HTTP/1.1" + LINE_SEPARATOR + LINE_SEPARATOR );
        Assert.equals( 4242, response.status );
        rc = Unserializer.run( response.content.toString() );
        Assert.equals( "/foo/bar", rc.resource );

        response = request( "GET /foo/bar? HTTP/1.1" + LINE_SEPARATOR + LINE_SEPARATOR );
        Assert.equals( 4242, response.status );
        rc = Unserializer.run( response.content.toString() );
        Assert.equals( "/foo/bar", rc.resource );

        response = request( "GET /foo/bar%20baz HTTP/1.1" + LINE_SEPARATOR + LINE_SEPARATOR );
        Assert.equals( 4242, response.status );
        rc = Unserializer.run( response.content.toString() );
        Assert.equals( "/foo/bar baz", rc.resource );

        response = request( "GET /foo/bar baz HTTP/1.1" + LINE_SEPARATOR + LINE_SEPARATOR );
        Assert.equals( 400, response.status );
    }
}
