package;

class TestTools {
    private function new(){}

    public static function count<T>( it : Iterator<T> ) : Int {
        var cnt = 0;
        for( i in it ) cnt++;
        return cnt;
    }

    public static function isEmpty<K,V>( map : Map<K,V> ) : Bool {
        return count( map.keys() ) == 0;
    }
}
