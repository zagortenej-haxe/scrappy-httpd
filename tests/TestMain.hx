package;

import utest.UTest;

class TestMain {
    public static function main() {

        UTest.run([
            new TestSmoke(),
            new TestBase(),
            new TestScrappy(),
            new TestResourceParser(),
        ]);

    }
}
