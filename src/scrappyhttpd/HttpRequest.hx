// Copyright 2024, Ravendyne Inc
// SPDX-License-Identifier: MIT

package scrappyhttpd;

import scrappyhttpd.server.ScrappyTypes;


class HttpRequest extends sys.Http {

    function verbToString( method : HttpVerb ) : String {
        return switch(method) {
            case GET: "GET";
            case HEAD: "HEAD";
            case POST: "POST";
            case PUT: "PUT";
            case DELETE: "DELETE";
            case CONNECT: "CONNECT";
            case OPTIONS: "OPTIONS";
            case TRACE: "TRACE";
        
            case PATCH: "PATCH";
        }
    }

    public function send( method : HttpVerb ) {
        var output = new haxe.io.BytesOutput();
        var oldOnError = onError;
        var err = false;
        onError = (e) -> {
            responseBytes = output.getBytes();
            err = true;
            onError = oldOnError;
            onError( e );
        }
        customRequest( false, output, verbToString( method ) );
        if( ! err ) {
            success( output.getBytes() );
        }
    }
}
