// Copyright 2023 - 2024, Ravendyne Inc
// SPDX-License-Identifier: MIT

package scrappyhttpd.server;

import sys.thread.Lock;
import haxe.Exception;
import haxe.io.Bytes;
import sys.thread.FixedThreadPool;
import sys.net.Host;
import sys.net.Socket;
import sys.io.File;
import haxe.io.Path;
import sys.FileSystem;

import scrappyhttpd.server.ScrappyTypes;

using StringTools;
using rxtoolbox.MiscTools;
using rxtoolbox.LogTools;
using scrappyhttpd.server.ScrappyTools;

class ScrappyHttpd extends ScrappyHttpdBase {
    static inline final CONCURRENCY = 5;

    var http_docs : String;

    var threadPool : FixedThreadPool;
    var socket : Socket;
    var serverStartLock : Lock;
    var serverStopLock : Lock;

    public var port(default,null) : Int;

    public function new( doc_root : String, ?log_output : HttpLog ) {
        super( log_output );
        if( doc_root == null ) {
            throw new Exception("'doc_root' is null");
        }
        // 6 MB
        max_payload_size = 6 * 1024 * 1024;
        http_docs = doc_root;
        socket = null;
        port = 0;
        serverStartLock = new Lock();
        serverStopLock = new Lock();
        threadPool = new FixedThreadPool( CONCURRENCY + 1 );
    }

    public function waitForStart() {
        serverStartLock.wait();
    }

    public function waitForStop() {
        serverStopLock.wait();
    }

    public function start( host : Host, port : Int, blocking : Bool = true ) {
        if( socket != null ) return;

        threadPool.run(() -> {

            socket = new Socket();
            socket.bind( host, port );
            socket.listen( CONCURRENCY );
            this.port = socket.host().port;
            log.info('Listening at http://${host.host}:${this.port}');
            serverStartLock.release();
            while(true) {
                try {
                    var client = socket.accept();
                    threadPool.run(() -> {
                        try {
                            client_handler( client );
                        } catch( ex : Exception ) {
                            log.error( ex.message );
                            log.error( ex.stack.toString() );
                        }
                    });
                } catch(e) {
                    log.debug('SERVER EXCEPTION ${e.details()}');
                    log.debug('SERVER EXCEPTION ${Std.string(e.native)}');
                    try socket.close() catch(e){}
                    break;
                }
            }
            serverStopLock.release();
            // trace('loop stopped');
        });

        if( blocking ) waitForStop();
    }

    public function stop() {
        if( socket == null ) return;

        socket.shutdown(true,true);
        socket.close();
        threadPool.shutdown();
        // serverStopLock.release();
    }

    function client_handler( client : Socket ) {
        // trace('handle_client ${client.peer().host.host}:${client.peer().port}');

        client.setTimeout( ScrappyHttpdBase.READ_TIMEOUT_SECONDS );
        // client.setBlocking( false );

        process_request( client.input, client.output );

        // trace('close client');
        client.close();
    }

    override function parse_query_text( q : String, params : StringMultiMap ) : Void {
        // https://www.rfc-editor.org/rfc/rfc3986.html#section-3.4
        if( null == q || q.length == 0 ) return;

        // simple implementation that understands key=value pairs
        // separated by & where multiple occurences of the same key
        // part will result in having multiple values stored under
        // parameter with that key.
        //
        // - key part CAN'T be empty, and if it is it will be ignored,
        // - value part CAN be empty, and if it is it will result in
        // an empty string
        // - key or value parts CAN'T have '=' in them, and if they do
        // only the first '=' will be interpreted as key/value separator
        // and everything between the second '=' and the first following '&'
        // will be ignored
        for( kv in q.split("&") ) {
            var p = kv.split("=");
            var key = p[0];
            if( key.length == 0 ) continue;
            var val = p.length > 1 ? p[1] : "";
            params.set( key, null != val ? val.urlDecode() : null );
        }
    }

    override function default_request_handler( request : HttpRequest ) : HttpResponse {

        var method = request.request_line.method;
        // remove leading /
        var resource = request.request_line.request_target.substr(1);
        resource = resource.urlDecode();
        // var request_headers = request.headers;

        var response_headers : HeadersMultiMap = HeadersMultiMap.EMPTY;

        switch( method ) {
            // https://www.rfc-editor.org/rfc/rfc9110#section-9.1
            case GET: {
                log.info(' >> GET: ' + '"${resource}"');
                var res_path = get_resource_headers_and_path( resource, response_headers );
                // get_resource_headers_and_path() checks if res_path exists
                // and throws 404 if it doesn't
                var response_content = File.getBytes( res_path );
                return {
                    status: 200,
                    status_text: 200.status_message(),
                    headers: response_headers,
                    content: response_content,
                }
            }

            case HEAD: {
                log.debug(' >> HEAD');
                get_resource_headers_and_path( resource, response_headers );
                return {
                    status: 200,
                    status_text: 200.status_message(),
                    headers: response_headers,
                    content: Bytes.alloc(0),
                }
            }

            default: {
                log.warn(' >> Unsupported "${method}"');
                return {
                    status: 501,
                    status_text: 501.status_message(),
                    headers: response_headers,
                    content: Bytes.alloc(0),
                }
            }
        }

        return {
            status: 404,
            status_text: 404.status_message(),
            headers: response_headers,
            content: Bytes.alloc(0),
        }
    }

    function get_resource_headers_and_path( resource : String, response_headers : HeadersMultiMap ) : String {
        if( resource.length == 0 ) {
            resource = "/";
        }
        if( resource.endsWith("/") ) {
            resource = resource + "index.html";
        }

        var res_path = Path.normalize( Path.join([ http_docs, resource ]) );
        // log.println(res_path + ' : ' + http_docs + ' : ' + resource);
        if( ! res_path.startsWith( http_docs ) ) {
            // access should only be allowed under http_docs folder
            throw new HttpError( 404 );
        }
        if( res_path.contains( ".." ) ) {
            // just in case res_path starts with http_docs
            // but somehow still contains '..' even after Path.normalize()
            throw new HttpError( 404 );
        }

        log.debug('resource: "${res_path}"');
        if( FileSystem.exists( res_path ) && ! FileSystem.isDirectory( res_path ) ) {
            var content_type = res_path.content_type();
            response_headers.set('Content-Type', content_type);
            // we don't set content length, the base class implementation does that
            // headers.set('Content-Length', ['${response_content.length}']);
        } else {
            throw new HttpError( 404 );
        }

        return res_path;
    }

}
