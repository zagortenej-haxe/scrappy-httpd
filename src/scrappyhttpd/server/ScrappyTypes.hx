// Copyright 2023 - 2024, Ravendyne Inc
// SPDX-License-Identifier: MIT

package scrappyhttpd.server;

import sys.thread.Mutex;
import haxe.io.Bytes;
import haxe.ds.StringMap;
import haxe.Exception;

using StringTools;
using scrappyhttpd.server.ScrappyTools;


// https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods
// https://www.rfc-editor.org/rfc/rfc9110#section-9.1
enum abstract HttpVerb(Int) {
    var GET;
    var HEAD;
    var POST;
    var PUT;
    var DELETE;
    var CONNECT;
    var OPTIONS;
    var TRACE;

    var PATCH; // https://www.rfc-editor.org/rfc/rfc5789
}

class StringMultiMap {
    public static var EMPTY(get,never) : StringMultiMap;
    public static function get_EMPTY() : StringMultiMap {
        return new StringMultiMap();
    }
    var storage : haxe.ds.StringMap<Array<String>>;
    function new() {
        storage = new haxe.ds.StringMap<Array<String>>();
    }
    function toTitleCase( v : String ) : String {
        if( null == v ) return null;
        if( v.length == 0 ) return "";
        return v.charAt(0).toUpperCase() + v.substr(1).toLowerCase();
    }
    function tag( key : String ) : String {
        return key;
    }
    public function set( key : String, value : String ) : Void {
        var tag = tag( key );
        if( ! storage.exists( tag ) ) {
            storage.set( tag, [] );
        }
        storage.get( tag ).push( value );
    }
    public function get( key : String ) : Null<String> {
        var tag = tag( key );
        if( ! storage.exists( tag ) ) return null;
        var v = storage.get( tag );
        if( v.length == 0 ) return null;
        return v[0];
    }
    public function getAll( key : String ) : Array<String> {
        var tag = tag( key );
        if( ! storage.exists( tag ) ) return [];
        return storage.get( tag );
    }
    public function exists( key : String ) : Bool {
        return storage.exists( tag( key ) );
    }
    public function remove( key : String ) : Bool {
        return storage.remove( tag( key ) );
    }
    public function keys() : Iterator<String> {
        return storage.keys();
    }
    public function iterator() : Iterator<Array<String>> {
        return storage.iterator();
    }
    public function keyValueIterator() : KeyValueIterator<String, Array<String>> {
        return storage.keyValueIterator();
    }
    public function copy() : StringMultiMap {
        var c = storage.copy();
        var n = new StringMultiMap();
        n.storage = c;
        return n;
    }
    public function toString() : String {
        return storage.toString();
    }
    public function clear() : Void {
        storage.clear();
    }
    public function isEmpty() : Bool {
        return ! storage.keys().hasNext();
    }
}

class HeadersMultiMap extends StringMultiMap {
    public static var EMPTY(get,never) : HeadersMultiMap;
    public static function get_EMPTY() : HeadersMultiMap {
        return new HeadersMultiMap();
    }
    override function tag( key : String ) : String {
        var parts = key.split('-');
        parts = [for(p in parts) toTitleCase(p)];
        return parts.join('-');
    }
}

typedef ParsedRequestLine = {
    var request_line : HttpRequestLine;
    var query : String;
    var fragment : String;
    var query_parameters : StringMultiMap;
}

enum abstract HttpVersion(String) to String {
    var HTTP_1_0 =  'HTTP/1.0';
    var HTTP_1_1 =  'HTTP/1.1';
}

typedef HttpRequestLine = {
    var method : HttpVerb;
    var request_target : String;
    var http_version : String;
}

typedef HttpRequest = {
    var request_line : HttpRequestLine;
    var query : String;
    var fragment : String;
    var query_parameters : StringMultiMap;
    var headers : HeadersMultiMap;
    var content : Bytes;
}

typedef HttpResponse = {
    var status : Int;
    var status_text : String;
    var headers : HeadersMultiMap;
    var content : Bytes;
}

class HttpError extends Exception {
    public var status(default,null) : Int;
    public var status_text(default,null) : String;
    public function new( status : Int, ?text : String ) {
        this.status = status;
        this.status_text = text != null ? text : status.status_message();
        super('${status} ' + text);
    }
}

class ConnectionTimeout extends Exception {
    public function new() {
        super('timeout');
    }
}

// TODO model per api gw lambda integration request
// https://docs.aws.amazon.com/lambda/latest/dg/services-apigateway.html#apigateway-example-event
typedef APIRequest = {
    var resourcePath : String;
    var httpMethod : HttpVerb;
    var query : String;
    var fragment : String;
    var queryParameters : StringMultiMap;
    var pathParameters : StringMap<String>;
    var headers : HeadersMultiMap;
    var ?body : Bytes;
}

// TODO model per api gw lambda integration response
// https://docs.aws.amazon.com/lambda/latest/dg/services-apigateway.html#apigateway-types-transforms
typedef APIResponse = {
    var statusCode : Int;
    var ?headers : HeadersMultiMap;
    var ?data : Bytes;
}

typedef APIHandler = APIRequest -> APIResponse;

// model per openapi + aws gw proxy integration
// https://docs.aws.amazon.com/apigateway/latest/developerguide/http-api-develop-integrations-lambda.html
typedef APIRegistration = {
    var path : String;
    // var parameters : TBD;
    var handler : APIHandler;
}

class HttpLog extends rxtoolbox.LogBase {
    private var mutex : Mutex;
    override function lock() {
        mutex.acquire();
    }

    override function unlock() {
        mutex.release();
    }
}
