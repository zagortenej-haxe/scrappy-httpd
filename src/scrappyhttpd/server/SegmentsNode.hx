// Copyright 2023 - 2024, Ravendyne Inc
// SPDX-License-Identifier: MIT

package scrappyhttpd.server;

import haxe.ds.StringMap;

using StringTools;


enum abstract SegmentType(Int) {
    var LITERAL;
    var PARAMETER;
}

typedef SegmentsMatch<T> = {
    var handlers : Array<T>;
    var params : StringMap<String>;
}

class SegmentsNode<T> {
    public var type(default,null) : SegmentType;
    public var value(default,null) : String;

    public var children(default,null) : Array<SegmentsNode<T>>;
    public var literals(default,null) : Array<String>;
    public var params(default,null) : Array<String>;

    public var handlers(default,null) : Array<T>;

    public function new( ?segment : String ) {
        children = [];
        literals = [];
        params = [];
        handlers = [];
        if( segment == null ) segment = "";
        if( segment.startsWith("{") && segment.endsWith("}") ) {
            type = PARAMETER;
            value = segment.substring( 1, segment.length - 1 );
        } else {
            type = LITERAL;
            value = segment;
        }
    }

    public function register( path_value : String, ?handler : T ) : Void {
        add( make_segments( path_value ), handler );
    }

    public function getHandlers( path : String ) : Null<SegmentsMatch<T>> {
        return match( make_segments( path ) );
    }

    function addHandler( ?handler : T ) {
        if( null == handler ) return;
        handlers.push( handler );
    }

    function equals( other : SegmentsNode<T> ) : Bool {
        return type == other.type && value == other.value;
    }

    function find( like : SegmentsNode<T> ) : Null<SegmentsNode<T>> {
        for( c in children ) {
            if( c.equals( like ) ) return c;
        }
        return null;
    }

    function findByType( type : SegmentType, value : String ) : Null<SegmentsNode<T>> {
        for( c in children ) {
            if( type == c.type && value == c.value ) return c;
        }
        return null;
    }

    function make_segments( path_value : String ) : Array<String> {

        // "" path is not allowed, gotta start with /
        if( ! path_value.startsWith("/") ) return [];

        var segments = path_value.split("/");

        if( segments.length > 1 && segments[ segments.length - 1 ] == "" ) {
            // remove last empty segment
            // effectively removes trailing /
            segments = segments.slice( 0, segments.length - 1 );
        }

        if( segments.length > 1 ) {
            // effectively removes leading /
            // but only if there's something after it
            segments = segments.slice(1);
        }

        return segments;
    }

    function add( segments : Array<String>, ?handler : T ) : Void {
        if( segments.length == 0 ) return;

        var head = new SegmentsNode( segments[0] );
        var tail = segments.slice( 1 );

        var c = find( head );
        if( null == c ) {
            switch( head.type ) {
                case LITERAL: literals.push( head.value );
                case PARAMETER: params.push( head.value );
            }
            children.push( head );

        } else {
            head = c;
        }

        if( tail.length == 0 ) {
            head.addHandler( handler );
        }
        head.add( tail, handler );
    }

    function match( segments : Array<String>, ?p : StringMap<String> ) : Null<SegmentsMatch<T>> {
        if( null == p ) p = new StringMap();
        if( segments.length == 0 ) return {
            handlers: handlers,
            params: p,
        };

        var head = segments[0];
        var tail = segments.slice( 1 );

        // literal match has precendence
        // so check those first
        if( literals.contains( head ) ) {
            // exact segment match
            var reg = findByType( LITERAL, head );
            return reg.match( tail, p );
        }

        // no literal match, treat the segment as parameter
        // value and try parameters
        for( pname in params ) {
            var reg = findByType( PARAMETER, pname );
            var m = reg.match( tail, p );
            // the first match wins
            if( m != null ) {
                p.set( pname, head );
                return {
                    handlers: m.handlers,
                    params: p,
                }
            }
        }

        return null;
    }
}
