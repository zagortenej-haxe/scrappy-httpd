// Copyright 2023 - 2024, Ravendyne Inc
// SPDX-License-Identifier: MIT

package scrappyhttpd.server;

import haxe.io.BytesBuffer;
import haxe.io.Output;
import haxe.io.Bytes;
import haxe.io.Input;

import scrappyhttpd.server.ScrappyTypes;

using StringTools;
using rxtoolbox.MiscTools;
using rxtoolbox.LogTools;
using scrappyhttpd.server.ScrappyTools;

typedef APIRegistryNode = SegmentsNode<APIHandler>;

// https://developer.mozilla.org/en-US/docs/Web/HTTP/Messages
class ScrappyHttpdBase {
    static inline final READ_TIMEOUT_SECONDS = 5;
    static inline final READ_REQUESTS_COUNT = 5;
    static inline final LINE_SEPARATOR = '\r\n';
    var NO_CONTENT(get,null):Bytes;
    function get_NO_CONTENT():Bytes {return Bytes.alloc(0);}

    var apis : APIRegistryNode;

    var api_resource_base : String;
    var max_payload_size : Int;

    var log : HttpLog;

    public function new( ?httpLog : HttpLog ) {
        log = httpLog == null ? new NullHttpLog() : httpLog;
        apis = new APIRegistryNode();
        api_resource_base = "api";
        // 2 GB, just an arbitrary number, fits in 32-bit int
        max_payload_size = 2 * 1024 * 1024 * 1024;
    }

    public function registerAPI( api : APIRegistration ) {
        apis.register( api.path, api.handler );
    }

    public function registerAPIs( apis : Array<APIRegistration> ) {
        for( api in apis ) registerAPI( api );
    }

    public function process_request( input : Input, output : Output ) : Void {
        var request : Null<HttpRequest> = null;
        var response : Null<HttpResponse> = null;
        var number_of_requests_left = READ_REQUESTS_COUNT;

        do {
            log.debug('requests left ${number_of_requests_left}');
            number_of_requests_left--;

            try {
                // TODO:
                // if protocol is HTTP/1.1, check for Connection header and
                // return Keep-Alive header with 'timeout' set to READ_TIMEOUT_SECONDS
                // and 'max' set to READ_REQUESTS_COUNT and then parse READ_REQUESTS_COUNT
                // requests
                // if protocol is HTTP/1.0, hangup after processing each request
                request = parse_request( input );

                response = handle_request( request );

                log.debug('responded to ${number_of_requests_left + 1}');

            } catch( ex : ConnectionTimeout ) {
                log.debug('closing connection after timeout, ${number_of_requests_left + 1}');
                break;
            } catch( ex ) {
                log.error('handle request exception');
                log.error( ex.toString() );
                log.error( ex.stack.toString() );
                response = {
                    status: 500,
                    status_text: ScrappyTools.status_message( 500 ),
                    headers: HeadersMultiMap.EMPTY,
                    content: NO_CONTENT,
                }
            }

            if( log.isDebug ) {
                log.lock();
                log.debug('>> RESPONSE HEADERS START');
                for( hdr_name in response.headers.keys() ) {
                    var hdr_values = response.headers.getAll(hdr_name);
                    for( hdr_value in hdr_values ) {
                        log.debug('${hdr_name}: ${hdr_value}');
                    }
                }
                log.debug('>> RESPONSE HEADERS END');
                log.unlock();
            }


            if( request != null ) {
                // https://datatracker.ietf.org/doc/html/rfc2616#section-8
                // https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Connection
                // https://httpwg.org/specs/rfc9110.html#field.connection
                // https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Keep-Alive
                // https://www.rfc-editor.org/rfc/rfc2068.html#section-19.7.1

                var connection_header = request.headers.get('connection');
                if( null != connection_header ) {
                    var parts = connection_header.split(',').map( v -> v.trim().toLowerCase() );

                    if( parts.contains('keep-alive') ) {
                        log.debug('persistent connection requested');
                    }
                    if( parts.contains('close') ) {
                        log.debug('closing connection on client request');
                        number_of_requests_left = 0;
                    }
                }

                // HTTP/1.0 client
                if( request.request_line.http_version == HTTP_1_0 ) {
                    number_of_requests_left = 0;
                }

                // HTTP/1.1 client
                if( request.request_line.http_version == HTTP_1_1 ) {
                    // connection is persistent by default, and 
                    // keep-alive header has no defined values so we shouldn't use it and
                    // just send Connection: close once we serve predefined number
                    // of requests
                }
            }

            // TODO adjust this based on if we are using persisten connection
            if( number_of_requests_left <= 0 ) {
                response.headers.set('Connection', 'close');
            } else {
                // HTTP/1.1 does not define any keep-alive header parameter values
                // keep-alive should be used only for HTTP/1.0 if client sends Connection: keep-alive
                // which means it is trying to negotiate persistent connection with HTTP/1.0 server
                // https://datatracker.ietf.org/doc/html/rfc2068#section-19.7.1
                // and
                // https://datatracker.ietf.org/doc/html/rfc2068#section-19.7.1.1
                // response.headers.set('Keep-Alive', 'timeout=${READ_TIMEOUT_SECONDS}, max=${number_of_requests_left}');
            }

            write_response( output, response );

        } while( number_of_requests_left > 0 );
    }

    function write_response( output : Output, response : HttpResponse ) : Void {

        // response line
        output.writeString('HTTP/1.1 ${response.status} ${response.status_text}');
        output.writeString(LINE_SEPARATOR);

        // header lines
        for( header_name => header_values in response.headers.keyValueIterator() ) {
            if( 'content-length' == header_name.toLowerCase().trim() ) continue;
            for( value in header_values ) {
                output.writeString(header_name);
                output.writeString(': ');
                output.writeString(value);
                output.writeString(LINE_SEPARATOR);
            }
        }

        // TODO content encoding and stuff
        if( response.content != null/*  && response.content.length > 0 */ ) {
            output.writeString('Content-Length: ${response.content.length}');
            output.writeString(LINE_SEPARATOR);
        }

        // end response + header lines
        output.writeString(LINE_SEPARATOR);

        // response content
        if( response.content != null ) {
            // TODO content encoding and stuff
            output.write(response.content);
        }
    }

    public function handle_request( request : HttpRequest ) : HttpResponse {

        var response : HttpResponse;

        try {

            var request_resource = request.request_line.request_target;
            var api_base = '/${api_resource_base}';
            if( request_resource.startsWith( api_base ) ) {
                // strip api_base
                request_resource = request_resource.substr( api_base.length );
                // prefix with '/' if needed
                if( ! request_resource.startsWith('/') ) {
                    request_resource = '/' + request_resource;
                }

                response = handle_api_request({
                    request_line: {
                        // send request with modified request_resource
                        request_target: request_resource,
                        method: request.request_line.method,
                        http_version: request.request_line.http_version,
                    },
                    query: request.query,
                    fragment: request.fragment,
                    query_parameters: request.query_parameters,
                    headers: request.headers,
                    content: request.content,
                });
            } else {
                response = default_request_handler( request );
            }

        } catch( ex : HttpError ) {
            response = {
                status: ex.status,
                status_text: ex.status_text,
                headers: HeadersMultiMap.EMPTY,
                content: NO_CONTENT,
            }
        }

        return response;
    }

    function default_request_handler( request : HttpRequest ) : HttpResponse {
        return {
            status: 404,
            status_text: 404.status_message(),
            headers: HeadersMultiMap.EMPTY,
            content: NO_CONTENT,
        }
    }

    function parse_start_line( line : String ) : ParsedRequestLine {

        // HTTP/1.1: https://datatracker.ietf.org/doc/html/rfc9112

        // Request Line: https://tools.ietf.org/html/rfc7230#section-3.1.1
        var re = new EReg(
        "^(GET|HEAD|POST|PUT|DELETE|CONNECT|OPTIONS|TRACE|PATCH|PRI) " +
        "(" +
        "([^ ?#]+)" + // path
        "(?:\\?([^ #]*))?" + // query
        "(?:#(.*))?" + // fragment
        ")" +
        " (HTTP/[12]\\.[01])$", "i");

        if( re.match( line ) ) {
            var version = re.matched( 6 );
            var method = re.matched( 1 );
            // Request Target (origin-form): https://www.rfc-editor.org/rfc/rfc7230.html#section-5.3
            // Path: https://www.rfc-editor.org/rfc/rfc3986.html#section-3.3
            var request_target = re.matched( 2 ); // full request path
            var path = re.matched( 3 ); // just the path, without query
            var query = re.matched( 4 ); // the query part
            var fragment = re.matched( 5 ); // the fragment part

            // https://datatracker.ietf.org/doc/html/rfc7230#section-5.3.1
            // origin-form   = absolute-path [ "?" query ]
            // absolute-path = 1*( "/" segment )
            var request_resource = path;
            if( ! request_resource.startsWith("/") ) {
                throw new HttpError( 400 );
            }
            request_resource = request_resource.urlDecode();

            var params = StringMultiMap.EMPTY;
            // Parse query text
            query = null == query ? "" : query;
            query = query.urlDecode();
            parse_query_text( query, params );

            fragment = null == fragment ? "" : fragment;
            fragment = fragment.urlDecode();

            return {
                request_line: {
                    method: switch( method ) {
                        case 'GET': GET;
                        case 'HEAD': HEAD;
                        case 'POST': POST;
                        case 'PUT': PUT;
                        case 'DELETE': DELETE;
                        case 'CONNECT': CONNECT;
                        case 'OPTIONS': OPTIONS;
                        case 'TRACE': TRACE;
                        case 'PATCH': PATCH;
                        default: throw new HttpError( 400 );
                    },
                    request_target: request_resource,
                    http_version: version,
                },
                query: query,
                fragment: fragment,
                query_parameters: params,
            }
        }

        throw new HttpError( 400 );
    }

    function parse_query_text( q : String, params : StringMultiMap ) : Void {
        // noop
        // query string format is not defined by RFC
        // and depends on the server
    }

    function parse_field_lines( lines : Array<String> ) : HeadersMultiMap {
        // https://www.rfc-editor.org/rfc/rfc9112.html#section-5
        var headers = HeadersMultiMap.EMPTY;

        for( line in lines ) {
            var separator_idx = line.indexOf(':');
            if( separator_idx < 0 ) {
                throw new HttpError( 400 );
            }

            var header_name = line.substring( 0, separator_idx ).toLowerCase();
            var header_value = line.substring( separator_idx + 1 ).trim();
            if( header_name != header_name.trim() ) {
                throw new HttpError( 400 );
            }

            headers.set( header_name, header_value );
        }

        return headers;
    }

    function parse_request( input : Input ) : HttpRequest {

        var request_header_lines : Array<String> = [];

        // https://www.rfc-editor.org/rfc/rfc9112.html#section-2

        // read start line
        // trace('read start line');
        var line = read_line( input );
        // TODO add client IP to the line, or
        // create a better formatted line for access logs
        log.info(line);
        var parsed_start_line = parse_start_line( line );
        var request_line = parsed_start_line.request_line;

        // 1.1 for keepalive connections
        if( request_line.http_version != "HTTP/1.0" && request_line.http_version != "HTTP/1.1" ) {
            throw new HttpError( 501 );
        }

        // read header lines
        // trace('read header lines');
        var line : String = null;
        while(true) {
            // client.waitForRead();
            try {
                line = read_line( input );
                // stop at the empty line
                if( line.length == 0 ) break;
                request_header_lines.push( line );
            } catch( ex : ConnectionTimeout ) {
                throw ex;
            } catch( ex : Dynamic ) {
                break;
            }
        }

        if( log.isDebug ) {
            log.lock();
            log.debug('>> REQUEST HEADERS START');
            for( line in request_header_lines ) {
                log.debug( line );
            }
            log.debug('>> REQUEST HEADERS END');
            log.unlock();
        }

        // there should be empty line ending the request head
        if( null == line || line.length > 0 ) {
            throw new HttpError( 400 );
        }

        // all good, we have empty line that marks
        // end of request head, now parse headers
        var request_headers = parse_field_lines( request_header_lines );

        // read request content, if any
        // trace('read request content');
        var max_size = max_payload_size;
        if( request_headers.exists('content-length') ) {
            var content_length = Std.parseInt( request_headers.get('content-length') );
            if( null != content_length && content_length < max_payload_size ) {
                max_size = content_length;
            }
        }

        // TODO DELETE may have body
        // https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods
        var request_content : Bytes;
        if( request_line.method == POST || request_line.method == PUT || request_line.method == PATCH ) {
            // trace('read payload');
            request_content = read_payload( input, max_size );
        } else {
            request_content = NO_CONTENT;
        }

        return {
            request_line: request_line,
            query: parsed_start_line.query,
            fragment: parsed_start_line.fragment,
            query_parameters: parsed_start_line.query_parameters,
            headers: request_headers,
            content: request_content,
        }
    }

    function read_payload( input : Input, byte_count : Int ) : Bytes {
        var data = new BytesBuffer();
        var buf = Bytes.alloc(8192);
        try {
            while( byte_count > 0 ) {
                var len_read = input.readBytes( buf, 0, buf.length );
                data.addBytes( buf, 0, len_read );
                byte_count -= len_read;
            }
        // } catch( e : Eof ) {}
        } catch( e : Dynamic ) {} // Eof doesn't work with timeouts
        return data.getBytes();
    }

    function read_line( input : Input ) : String {

        var buf = new BytesBuffer();
		var last : Int;
		var s;

        try {

            while( ( last = input.readByte() ) != "\n".code ) {
                buf.addByte(last);
            }

            s = buf.getBytes().toString();

            if( s.charCodeAt(s.length - 1) != "\r".code )
                // incorrectly terminated line
                throw new HttpError( 400 );
            else
                s = s.substr(0, -1);
        } catch ( e : HttpError ) {
            throw e;
        // } catch ( e : Eof ) {
        } catch ( e : Dynamic ) { // Eof doesn't work with timeouts
            throw new ConnectionTimeout();
		}

        return s;
    }

    function handle_api_request( request : HttpRequest ) : HttpResponse {
        var resourcePath = request.request_line.request_target;
        if( resourcePath == null ) resourcePath = "/";

        var api = apis.getHandlers( resourcePath );

        if( api == null || api.handlers.length == 0 ) {
            return {
                status: 404,
                status_text: 404.status_message(),
                headers: HeadersMultiMap.EMPTY,
                content: NO_CONTENT,
            }
        }

        var apiRequest : APIRequest = {
            resourcePath: resourcePath,
            httpMethod: request.request_line.method,
            headers: request.headers,
            query: request.query,
            fragment: request.fragment,
            queryParameters: request.query_parameters,
            pathParameters: api.params,
            body: request.content,
        }

        var apiResponse : APIResponse;
        try {
            // TODO chain api handler calls to get response
            apiResponse = api.handlers[0]( apiRequest );
        } catch(e) {
            log.error('Exception: API handler for "${resourcePath}"');
            log.error(e.stack.toString());
            apiResponse = {
                statusCode: 500,
                data: Bytes.ofString('{"error":"${e}"}'),
            }
        }

        var headers : HeadersMultiMap = apiResponse.headers != null ? apiResponse.headers : HeadersMultiMap.EMPTY;
        if( apiResponse.data != null && ! headers.exists('Content-Type') ) {
            headers.set('Content-Type', 'application/json');
        }

        return {
            status: apiResponse.statusCode,
            status_text: apiResponse.statusCode.status_message(),
            headers: headers,
            content: apiResponse.data,
        }
    }
}
