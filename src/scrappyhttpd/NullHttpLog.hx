// Copyright 2024, Ravendyne Inc
// SPDX-License-Identifier: MIT

package scrappyhttpd;

import scrappyhttpd.server.ScrappyTypes.HttpLog;
import haxe.io.Output;

class NullOutput extends Output {
    public function new() {}
    override public function writeByte( c: Int ) : Void {
        // noop
    }
}

class NullHttpLog extends HttpLog {
    public function new() {
        super( new NullOutput() );
    }
}
