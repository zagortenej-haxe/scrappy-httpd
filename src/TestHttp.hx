// Copyright 2024, Ravendyne Inc
// SPDX-License-Identifier: MIT

package;

import scrappyhttpd.HttpRequest;

// haxe -cp src --run TestHttp

class TestHttp {
    public static function main() {
        getRequest();
        // postRequest();
    }

    // https://go.postman.co/postman/workspace/published-postman-templates/documentation/631643-f695cab7-6878-eb55-7943-ad88e1ccfd65?ctx=documentation&entity=request-1eb1cf9d-2be7-4060-f554-73cd13940174
    static function postRequest() {
        var r = null;
        // var req = new sys.Http('https://postman-echo.com/post');
        var req = new HttpRequest('https://postman-echo.com/post');
        req.onData = (data:String) -> {
            r = data;
        }
        req.onError = (msg) -> {
            Sys.println(msg);
        }
        req.setHeader("Content-Type","application/json");
        req.setPostData('{"test":"value"}');
        // it will POST if post data is set
        // req.request(/* true */);
        req.send( POST );
        Sys.println(req.responseHeaders);
        Sys.println('---------------');
        Sys.println(r);
    }

    // https://go.postman.co/postman/workspace/published-postman-templates/documentation/631643-f695cab7-6878-eb55-7943-ad88e1ccfd65?ctx=documentation&entity=request-078883ea-ac9e-842e-8f41-784b59a33722
    static function getRequest() {
        var r = null;
        // var req = new sys.Http('https://postman-echo.com/get?foo1=bar1&foo2=bar2');
        // var req = new HttpRequest('https://postman-echo.com/get?foo1=bar1&foo2=bar2');
        var req = new HttpRequest('https://testapi.jasonwatmore.com/products');
        req.onData = (data:String) -> {
            r = data;
        }
        req.onError = (msg) -> {
            Sys.println(msg);
        }
        // req.request();
        req.send( GET );
        Sys.println(req.responseHeaders);
        Sys.println('---------------');
        Sys.println(r);
    }
}