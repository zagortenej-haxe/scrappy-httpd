// Copyright 2024, Ravendyne Inc
// SPDX-License-Identifier: MIT

package;

import rxtoolbox.cli.ProcRun;

using StringTools;

class AwsCli {
    var AWS = 'aws';
    var AWS_SETTINGS : Array<String> = [];
    public function new( region : String, profile : String = 'default' ) {
        AWS_SETTINGS.push('--output');
        AWS_SETTINGS.push('json');
        AWS_SETTINGS.push('--region');
        AWS_SETTINGS.push(region);
        AWS_SETTINGS.push('--profile');
        AWS_SETTINGS.push(profile);
    }

}

typedef AwsParamsS3apiListObjects = {
    var prefix: String;
}

class AwsS3api extends AwsCli {
    public function new( region : String, profile : String = 'default' ) {
        super( region, profile );
        AWS_SETTINGS.push('s3api');
    }

    // https://awscli.amazonaws.com/v2/documentation/api/latest/reference/s3api/list-objects.html
    public function listObjects( bucket : String, ?options : AwsParamsS3apiListObjects ) : ProcRunResult {

        var args = AWS_SETTINGS.copy();
        args.push('list-objects');

        args.push('--bucket');
        args.push(bucket);

        if( null != options ) {
            args.push('--prefix');
            args.push(options.prefix);
        }

        var rr = ProcRun.cmd( AWS, args );
        return rr;
    }
}

class TestProc {
    public static function main() {
        list_objects_api();
    }

    static function list_objects_api() {
        var s3 = new AwsS3api( 'us-east-1', 'dev' );
        var r = s3.listObjects( 'my-bucket', {prefix: 'my-prefix'} );
        trace(r);
    }

    static function list_objects() {
        var args = [
            "--region", "us-east-1",
            "--profile", "dev",
            // https://awscli.amazonaws.com/v2/documentation/api/latest/reference/s3api/list-objects.html
            "s3api",
            "list-objects",
            "--bucket", "survey-upload",
            "--prefix", "m20f",
        ];
    }
}
