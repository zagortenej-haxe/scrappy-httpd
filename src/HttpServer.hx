// Copyright 2023 - 2024, Ravendyne Inc
// SPDX-License-Identifier: MIT

package;

import scrappyhttpd.server.ScrappyTypes.HttpLog;
import scrappyhttpd.server.ScrappyHttpd;
import sys.net.Host;

class HttpServer {

    static public function main() {

        var host = new Host('127.0.0.1');
        var port = 0; // binding to port 0 will give us random free port
        var http_docs = ".";

        var args = Sys.args();
        if( args.length > 0 ) http_docs = args[0];
        if( args.length > 1 ) {
            var p = Std.parseInt( args[1] );
            if( null != p ) port = p;
        }

        Sys.println('Serving from "${http_docs}"');

        final log = new HttpLog( Sys.stdout() );
        // log.level = rxtoolbox.LogBase.LogLevel.DEBUG;
        var server = new ScrappyHttpd( http_docs, log );
        server.start( host, port );
    }
}